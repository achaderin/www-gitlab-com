---
layout: markdown_page
title: Managing Support contacts and handling details
description: "Taking control over who has access to your support entitlement"
---

The GitLab support team is here to help. To avoid any delays in processing your tickets we recommend seeding a list of 
named contacts. If a ticket is submitted by someone not on your list, we'll ask them to [prove their support entitlement](#proving-your-support-entitlement)
which will delay our initial response.

## On This Page
{:.no_toc}

- TOC
{:toc}

## Getting Set Up

Once your license or subscription is provisioned, we recommend submitting an initial ticket with a list of contacts who are allowed to contact Support.

1. Submit a Support ticket using the [Support portal related matters form](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360001801419) and select `First time setup` in the Problem type field.
1. You'll likely receive a reply letting you know that you need to [prove your support entitlement](#proving-your-support-entitlement), so be prepared with those details.

In the same ticket, you may also want to update ticket visibility by making a [shared organization](#shared-organizations), or lock any organization changes down by specifying a list of [authorized contacts](#authorized-contacts). If there are some [special handling notes](#special-handling-notes) you'd like for us to add, we're happy to do that as well.

### Proving your Support Entitlement

Depending on how you purchased GitLab, GitLab Support may not automatically detect your support entitlement on the creation of your first support ticket. If that's the case, you will be asked to provide evidence that you have an active license or subscription.

#### For GitLab.com Users
{:.no_toc}

To ensure that we can match you with your GitLab.com subscription when opening a support ticket, please:
- include your GitLab.com username; AND
- use the primary email address associated with your account; AND
- reference a path within a GitLab.com group of which you are a member and which has a valid subscription associated with it (e.g., a link to a problematic pipeline or MR)

#### For Self-managed Users
{:.no_toc}

To ensure that we can match you with your Self-managed license when opening a support ticket, please:
- use a company provided email address (no generic email addresses such as Gmail, Yahoo, etc.); AND
- provide GitLab with one of the following:
   - A screenshot of the license page `/admin/license` 
   - The output of the command `sudo gitlab-rake gitlab:license:info`
   - The license ID displayed on the `/admin/license` page (GitLab 13.2+)
   - Starting GitLab version 14.1 the license information can be found on the `/admin/subscription` page
   - The license file provided to you at the time of purchase **and at least one of these:**
     - the email address to which the license was issued
     - the company name to which the license was issued

## Managing contacts

Additional Support contacts can be managed by any user who can [prove their support entitlement](#proving-your-support-entitlement) by submitting a request using the [Support portal related matters form](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360001801419) and selecting `Manage my organization's contacts` in the Problem type field.

### Authorized contacts

For organizations that require additional security, you can specify a set of authorized contacts who can make changes.

1. Submit a Support ticket using the [Support portal related matters form](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360001801419) and select `Other requests` in the Problem type field.

We will add an internal note detailing who is allowed to make changes to the contacts in your organization.

## Shared Organizations

In some cases, certain organizations want all members of their organization to be able to see all of the support tickets that have been logged. In other cases, a particular user from the account would like to be able to see and respond to all tickets from their organization. If you'd like to enable this, please:

**Global Support Shared Organization Setup**
1. Submit a Support ticket using the Global [Support portal related matters form](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360001801419) and select `Shared organization requests` in the Problem type field.

**US Federal Support Shared Organization Setup**
1. Submit a Support ticket in the US Federal Support portal's [requests for shared organization setup form](https://federal-support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360001421052) and fill in what type of sharing you would like setup. Kindly note that if you wish to enable shared organizations in both portals you must submit a separate request using the Global Support portal form.

## Special Handling Notes

If there's a special set of handling instructions you'd like included in your Organizations notes, we'll do our best to comply. Not all requests can be accommodated, but
if there is something we can do to make your support experience better we want to know.

1. Submit a Support ticket using the [Support portal related matters form](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360001801419) and select `Other Requests` in the Problem type field.
